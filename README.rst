PyCultivator LED Panel Bindings
===============================

This package provides bindings to a multi-color LED Panel, developed by the University of Amsterdam, for the
pyCultivator project.

The Panel is driven with a Opal Kelly FPGA and the




FAQ
---

1. Python returns "Fatal Python error: PyThreadState_Get: no current thread"
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This error is caused by a wrongly linked shared object (`_ok.so`). To fix this we need to fix the links.

1. Locate the _ok.so file (installed with the package `pycultivator_led_panel/connection/ok`)
2. List shared libraries used by the module: `otool -L _ok.so`
3. Check Python library path (something like: `/System/Library/Frameworks/Python.framework/Versions/2.7/lib/libpython2.7.dylib`)
4. Find correct library path (for example with HomeBrew: `/usr/local/Cellar/python/2.7.13/Frameworks/Python
.framework/Versions/2.7/lib/libpython2.7.dylib`)
5. Correct shared library reference in `_ok.so` using `install_name_tool`:

`install_name_tool -change <current> <new>`

e.g.

`install_name_tool -change /System/Library/Frameworks/Python.framework/Versions/2.7/lib/libpython2.7.dylib
/usr/local/Cellar/python/2.7.13/Frameworks/Python.framework/Versions/2.7/lib/libpython2.7.dylib _ok.so`
