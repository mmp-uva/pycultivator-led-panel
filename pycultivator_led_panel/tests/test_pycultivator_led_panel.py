"""A global test script to test the functioning of the whole package"""

from pycultivator_led_panel.tests import LivePanelTestCase
from time import sleep

__author__ = "Joeri Jongbloets <j.a.jongbloets@uva.nl>"


class PackageTestCase(LivePanelTestCase):

    ALL_LEDS = range(8)
    TEST_PAUSE = 1
    TEST_LED_INTENSITY = 1000
    TEST_LED_PULSE_WIDTH = 100

    def test_load(self):
        self.assertEqual(len(self.getPanel().getInstruments().values()), len(self.ALL_LEDS))

    def test_leds(self):
        lamp = self.getPanel().getIdentity()
        intensity = self.TEST_LED_INTENSITY
        pwm = self.TEST_LED_PULSE_WIDTH
        # turn on
        for led in self.ALL_LEDS:
            with self.subTest("led ON #{}".format(led)):
                self.assertTrue(self.apply_led(lamp, led, intensity, pwm))
        # sleep
        sleep(self.TEST_PAUSE)
        # turn off
        for led in self.ALL_LEDS:
            with self.subTest("led OFF #{}".format(led)):
                self.assertTrue(self.apply_led(lamp, led, 0, 0))

    def test_measures(self):
        variables = self.getPanel().measures()
        self.assertEqual(variables, {"raw_intensity", "intensity", "permille_intensity", "pulse_width", "state"})

    def apply_led(self, lamp, led, intensity, pwm):
        connection = self.getPanel().getConnection()
        result = connection.setIntensity(lamp, led, intensity)
        result = result and connection.setPulseWidth(lamp, led, pwm)
        return result

    def test_controls(self):
        variables = self.getPanel().controls()
        self.assertEqual(variables, {"raw_intensity", "intensity", "permille_intensity", "pulse_width", "state"})

    def test_control(self):
        successes = self.getPanel().control("led", "intensity", 100)
        self.assertGreaterEqual(successes, 1)
        for success in successes:
            self.assertTrue(success)
        led = self.getPanel().getLEDs().values()[0]
        success = led.control("intensity", 50)
        self.assertTrue(success)
        self.assertEqual(led.measure("intensity"), 50)
        success = self.getPanel().control("led", "state", False)
        self.assertTrue(success)
        self.assertEqual(led.getPulseWidth(), 0)
