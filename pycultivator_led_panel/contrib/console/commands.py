"""Console Commands to manage a LED Panel"""


from pycultivator.contrib.console import commands, arguments, exceptions
from pycultivator.contrib.console.commands.device import DeviceCommand
from pycultivator.contrib.console.exceptions.device import *
from pycultivator_led_panel.panel import Panel


class PanelCommand(DeviceCommand):
    """Generic panel command

    Checks the type of the device used for this command
    """

    namespace = "panel"

    def execute(self):
        if not isinstance(self.device, Panel):
            raise InvalidDeviceException(self, self.device, Panel)
        return super(PanelCommand, self).execute()

# TODO: Add more panel commands


def load_commands():
    return [

    ]