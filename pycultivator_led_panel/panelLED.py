"""Implement the LED Instrument for the LED Panel"""

from pycultivator.core import Parser
from pycultivator.instrument.light import Light, LightException, ConstrainedInstrumentVariable

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class PanelLED(Light):
    """The PlateInstrument models an Instrument in the 24 well plate incubator"""

    namespace = "panel.instrument.led"

    COLOR_LIGHT_BLUE = "light-blue"
    COLOR_BLUE = "blue"
    COLOR_GREEN = "green"
    COLOR_YELLOW = "yellow"
    COLOR_ORANGE = "orange"
    COLOR_RED = "red"
    COLOR_VIOLET = "violet"
    COlOR_WHITE = "white"
    COLOR_CYAN = "cyan"

    COLOR_NAMES = (
        COLOR_LIGHT_BLUE, COLOR_BLUE, COLOR_GREEN, COLOR_ORANGE,
        COLOR_YELLOW, COLOR_RED, COLOR_VIOLET, COLOR_CYAN, COlOR_WHITE
    )

    default_settings = {
        # add default settings for this class
    }

    INSTRUMENT_TYPE_SYNONYMS = {
        "led"
    }

    # variables
    _permille_intensity = ConstrainedInstrumentVariable(
        value=0, minimum=0, maximum=1000, can_measure=True, can_control=True, private=True
    )

    # variables
    _pulse_width = ConstrainedInstrumentVariable(
        value=0, minimum=0, maximum=1000, can_measure=True, can_control=True, private=True
    )

    def __init__(self, identity, parent=None, settings=None, **kwargs):
        super(PanelLED, self).__init__(identity=identity, parent=parent, settings=settings, **kwargs)
        self._raw_intensity.maximum = 1000
        self._address = None

    # Getters / Setters

    def hasPanel(self):
        """Returns whether this LED is registered to a device"""
        return self.hasParent()

    def getPanel(self):
        """Returns the device object to which this instrument belongs.

        :rtype: pycultivator_led_panel.panel.Panel
        """
        return self.getParent()

    def hasAddress(self):
        """Returns whether this LED has a specifically set address.

        If false means; the result of getAddress is guessed using guessAddress

        :rtype: bool
        """
        return self.getSetting("address") is None

    def getAddress(self):
        """Returns the address of this LED.

        If the address is not set in the settings, the address will be guessed using guessAddress

        :rtype: int
        """
        result = self._getAddress()
        if result is None:
            result = self.guessAddress()
        return result

    def _getAddress(self):
        return self._address

    @property
    def address(self):
        return self.getAddress()

    def setAddress(self, address):
        """Sets a custom address for this LED (overrides guessing)

        :param address: The new address of the LED
        :type address: int
        :return: The new address of the LED
        :rtype: int
        """
        self._address = address

    @address.setter
    def address(self, address):
        self.setAddress(address)

    def setWavelength(self, nm):
        """Set the wavelength at which this LED emits in nanometers

        :type nm: int
        """
        ConstrainedInstrumentVariable.validate(nm, -1, 800, include=True, name="wavelength")
        super(PanelLED, self).setWavelength(nm)

    def setColor(self, c):
        if c is not None and c not in self.COLOR_NAMES:
            raise ValueError("Invalid color name: {}".format(c))
        super(PanelLED, self).setColor(c)

    def getPulseWidth(self, update=False):
        """Return the pulse width of the LED

        :rtype: float
        """
        if update:
            # no need
            value = self.read_led_pulse_width()
            self._pulse_width.set(value)
            self._pulse_width.commit()
        return self._pulse_width.get()

    def setPulseWidth(self, value, update=False):
        result = True
        # make integer
        if isinstance(value, (float, int)):
            value = round(value)
        self._pulse_width.set(value)
        if update:
            result = self.write_led_pulse_width(value)
            if result:
                self._pulse_width.commit()
        return result

    def getState(self, update=False):
        # LED Panel does not support states
        state = self.getRawIntensity(update=update) > 0
        self._state.set(state)
        if update:
            self._state.commit()
        return self._state.get()

    def getRawIntensity(self, update=False):
        # LED Panel does not support reading
        return self._raw_intensity.get()

    def getPermilleIntensity(self, update=False):
        value = self.getRawIntensity(update=update)
        self._permille_intensity.set(value)
        if update:
            self._permille_intensity.commit()
        return self._permille_intensity.get()

    def setPermilleIntensity(self, value, update=False):
        intensity = self._permille_intensity.constrain(value)
        result = self.setRawIntensity(intensity, update=update)
        if result:
            self._permille_intensity.set(intensity)
            if update:
                self._permille_intensity.commit()
        return result

    def testPermilleIntensity(self, value):
        result = False
        try:
            self._permille_intensity.validate(value)
            result = True
        except ValueError:
            pass
        return result

    #
    #   Communication hooks
    #

    def _measure(self, source, **kwargs):
        super(PanelLED, self)._measure(source, **kwargs)
        if source is self._permille_intensity:
            self.getPermilleIntensity(True)
        if source is self._pulse_width:
            self.getPulseWidth(True)

    def _control(self, source, value, **kwargs):
        super(PanelLED, self)._control(source, value, **kwargs)
        if source is self._permille_intensity:
            self.setPermilleIntensity(value, True)
        if source is self._pulse_width:
            self.setPulseWidth(value, True)

    def read_led_intensity(self):
        # return the stored value as the panel does not support reading!
        return self.getRawIntensity(False)

    def write_led_intensity(self, value):
        result = False
        if self.hasPanel() and self.getPanel().isConnected():
            connection = self.getPanel().getConnection()
            lamp = Parser.to_int(self.getPanel().getIdentity(), None)
            if lamp is None:
                raise PanelLEDException("Panel ID is invalid: {}".format(self.getPanel().getIdentity()))
            led = Parser.to_int(self.getIdentity(), None)
            if led is None:
                raise PanelLEDException("LED ID is invalid: {}".format(self.getIdentity()))
            result = connection.setIntensity(lamp, led, value)
        return result

    def read_led_pulse_width(self):
        return self._read_led_pulse_width()

    def _read_led_pulse_width(self):
        # return the stored value as the panel does not support reading!
        return self.getPulseWidth(False)

    def write_led_pulse_width(self, value):
        result = False
        if self.hasPanel() and self.getPanel().isConnected():
            connection = self.getPanel().getConnection()
            lamp = Parser.to_int(self.getPanel().getIdentity(), None)
            if lamp is None:
                raise PanelLEDException("Panel ID is invalid: {}".format(self.getPanel().getIdentity()))
            led = Parser.to_int(self.getIdentity(), None)
            if led is None:
                raise PanelLEDException("LED ID is invalid: {}".format(self.getIdentity()))
            result = connection.setPulseWidth(lamp, led, value)
        return result

    def read_led_state(self):
        # return stored value as the panel does not support reading!
        return self.getState(False)

    def write_led_state(self, state):
        if state:
            result = self.write_led_intensity(self.getRawIntensity(False))
            result = self.write_led_pulse_width(self.getPulseWidth(False)) and result
        else:
            result = self.write_led_intensity(0)
            result = self.write_led_pulse_width(0) and result
        return result

    # Behaviour methods

    def guessAddress(self):
        """Tries to guess the Sensor address from the index of the well where this instrument belongs to"""
        return Parser.to_int(self.getName(), default=None)


class PanelLEDException(LightException):
    """Exception raised by a PlatePhotoDiode Instrument"""

    pass
