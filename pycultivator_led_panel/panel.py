"""The Panel Device"""

from pycultivator.core import Parser
from pycultivator.device import device

from pycultivator_led_panel.connection.okConnection import OKConnection
from pycultivator_led_panel import panelLED

__author__ = "Joeri Jongbloets <j.a.jongbloets.net>"


class Panel(device.Device):
    """A panel consisting of instruments"""

    namespace = "led.panel"

    default_settings = {
        # add default settings for this class
    }

    # connection
    required_connection = OKConnection

    # instruments

    INSTRUMENTS = {
        panelLED.PanelLED: 8
    }

    def __init__(self, identity, parent=None, settings=None, **kwargs):
        super(Panel, self).__init__(identity, parent=parent, settings=settings, **kwargs)
        # set name as int
        self.setIdentity(identity)
        self._leds = {}  # name -> object

    def makeIdentity(self, name):
        name = Parser.to_int(name, default=0)
        return name

    def getConnection(self):
        """Returns the connection used to connect to the device

        :return: The connection being used.
        :rtype: pycultivator_led_panel.connection.okConnection.OKConnection
        """
        return super(Panel, self).getConnection()

    # instruments

    def getLED(self, name):
        """Return the led registered to this panel using the given name

        :rtype: pycultivator_led_panel.instrument.ledInstrument.LEDInstrument
        """
        if not self.hasLED(name):
            raise PanelException("Unable to retrieve LED: {} is not known".format(name))
        return self.getLEDs()[name]

    def hasLED(self, name):
        return name in self.getLEDs().keys()

    def getLEDs(self):
        """Returns all the registered LED Instruments

        :rtype: dict[str, pycultivator_led_panel.instrument.ledInstrument.LEDInstrument]
        """
        return self.getInstrumentsOfClass(panelLED.PanelLED)

    def getLEDColors(self):
        """ Get all colors that can be emitted by this Panel

        :param as_name: Whether to return the colors as strings or integers.
        :type as_name: bool
        :return:
        :rtype: list[str]
        """
        results = []
        for led in self.getLEDs().values():
            color = led.getColor()
            if color not in results:
                results.append(color)
        return results

    def getLEDsOfColor(self, color):
        """ Get all LEDs with the given color

        :param color: Color name or index
        :type color: str or int
        :return:
        :rtype: list[pycultivator_led_panel.instrument.ledInstrument.LEDInstrument]
        """
        results = []
        for led in self.getLEDs().values():
            if led.emitsColor(color):
                results.append(led)
        return results

    def hasLEDOfColor(self, color):
        return len(self.getLEDsOfColor(color)) > 0

    def getLEDWavelengths(self):
        """Get all the wavelengths that this panel can emit at

        :rtype: list[float]
        """
        results = []
        for led in self.getLEDs().values():
            nm = led.getWavelength()
            if nm not in results:
                results.append(nm)
        return results

    def getLEDsWithWavelength(self, nm):
        """Get all LEDs that can emit at the given wavelength

        :type nm: float
        :rtype: list[pycultivator_led_panel.instrument.ledInstrument.LEDInstrument]
        """
        results = []
        for led in self.getLEDs().values():
            if led.emitsWavelength(nm):
                results.append(led)
        return results

    def hasLEDOfWavelength(self, nm):
        """Return whether this panel has LED that emit at the given wavelength

        :type nm: float
        :rtype: bool
        """
        return len(self.getLEDsWithWavelength(nm)) > 0


class PanelException(device.DeviceException):
    """An exception raised by a LED Panel Object"""

    def __init__(self, msg):
        super(PanelException, self).__init__(msg)
