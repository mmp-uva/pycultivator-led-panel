"""Module implementing communiction to an Opal Kelly device"""

from pycultivator.core.pcVariable import ConstrainedVariable
from pycultivator.connection import connection
# from ok import ok
from okfrontpanel import fp
import pkg_resources, os

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class OKConnection(connection.Connection):
    """Class implementing communication to an Opal Kelly device over USB"""

    namespace = "connection.ok"
    default_settings = {
    }

    BIT_FILE_PATH = os.path.join("ok", "ow_pwm.bit")

    def __init__(self, settings=None, **kwargs):
        super(OKConnection, self).__init__(settings=settings, **kwargs)

    @classmethod
    def getFake(cls):
        return FakeOKConnection

    def getDevice(self):
        """Returns the device to which this connection goes

        :return: The LabJack Device
        :rtype: ok.okCFrontPanel
        """
        return super(OKConnection, self).getDevice()

    def _createDevice(self):
        """Creates a new connection handle"""
        # self._setDevice(ok.okCFrontPanel())
        self._setDevice(fp.FrontPanel())
        return self.getDevice()

    def configure(self, bitFile=None):
        if bitFile is None:
            bitFile = pkg_resources.resource_filename(self.__module__, self.BIT_FILE_PATH)
        if not os.path.exists(bitFile):
            raise OKConnectionException("Could not open BitFile at {}".format(bitFile))
        eCode = self._configure(bitFile)
        result = eCode == 0
        if not result:
            self.getLog().warning("Unable to configure FPGA: {}".format(eCode))
        return result

    def _configure(self, bitFile):
        return self.getDevice().ConfigureFPGA(bitFile)

    def connectWithSerialId(self, serialId, bitFile=None):
        return self.connect(bitFile=bitFile, serialId=serialId)

    def connect(self, bitFile=None, serialId=None):
        if self.isConnected():
            raise OKConnectionException("Already connected, disconnect first")
        if not self.hasDevice():
            self._createDevice()
        eCode = self._connect(serialId)
        result = eCode == 0
        if result:
            result = self.configure(bitFile)
            if result:
                self._isOpen = True
        else:
            self.getLog().warning("Unable to connect to FPGA: {}".format(result))
        return self.isConnected()

    def _connect(self, serialId=None):
        if serialId is None:
            serialId = ""
        return self.getDevice().OpenBySerial(serialId)

    def disconnect(self):
        result = False
        if not self.hasDevice():
            raise OKConnectionException(
                "No device connected, so nothing to disconnect"
            )
        if self.isConnected():
            eCode = self._disconnect()
            result = eCode == 0
            if result:
                self._isOpen = False
            else:
                self.getLog().warning("Unable to disconnect from FPGA: {}".format(eCode))
        return result

    def _disconnect(self):
        self.getDevice().Close()
        return 0

    def apply(self):
        result = self.update()
        result = result and self.trigger()
        return result

    def trigger(self):
        """Will trigger an update on the FPGA"""
        result = self._trigger()
        if result is None:
            result = 0
        if result < 0:
            self.getLog().warning("Unable to active trigger: {}".format(result))
        return result >= 0

    def _trigger(self):
        return self.getDevice().ActivateTriggerIn(0x40, 0)

    def update(self):
        """Will update the WireIn values"""
        result = self._update()
        if result is None:
            result = 0
        if result < 0:
            self.getLog().warning("Unable to update wireIns: {}".format(result))
        return result >= 0

    def _update(self):
        return self.getDevice().UpdateWireIns()

    def setWireIn(self, address, value):
        result = self._wireIn(int(address), int(value))
        if result < 0:
            self.getLog().warning("Unable to set wireIn at {} to {}: {}".format(
                address, value, result
            ))
        return result >= 0

    def _wireIn(self, address, value):
        return self.getDevice().SetWireInValue(address, value)

    def setIntensity(self, lamp, led, intensity):
        """Will set the intensity of one color"""
        ConstrainedVariable.validate(lamp, 0, 7, name="lamp")
        ConstrainedVariable.validate(led, 0, 7, name="led")
        ConstrainedVariable.validate(intensity, 0, (1 << 11)-1, name="intensity")
        result = self.setWireIn(0x11, (lamp * 8) + led + 8)
        result = result and self.setWireIn(0x10, intensity)
        result = result and self.apply()
        self.getLog().debug("Set intensity to {} in LED {}, lamp {}: {}".format(
            intensity, led, lamp, result
        ))
        return result

    def setAllIntensity(self, lamp, intensity):
        """Set the intensity of all leds in the lamp"""
        result = True
        for led in range(8):
            success = self.setIntensity(lamp, led, intensity)
            result = result and success
        return result

    def setPulseWidth(self, lamp, led, pwm):
        """Will set the pulse width of one led"""
        ConstrainedVariable.validate(lamp, 0, 7, name="lamp")
        ConstrainedVariable.validate(led, 0, 7, name="led")
        ConstrainedVariable.validate(pwm, 0, (1 << 11) - 1, name="pulse width")
        result = self.setWireIn(0x11, (lamp * 8) + led)
        result = result and self.setWireIn(0x10, pwm)
        result = result and self.apply()
        self.getLog().debug("Set pulse width to {} in LED {}, lamp {}: {}".format(
            pwm, led, lamp, result
        ))
        return result

    def setAllPulseWidth(self, lamp, pwm):
        """Set the pulse width of all leds in the lamp"""
        result = True
        for led in range(8):
            success = self.setPulseWidth(lamp, led, pwm)
            result = result and success
        return result


class OKConnectionException(connection.ConnectionException):

    def __init__(self, msg):
        super(OKConnectionException, self).__init__(msg)


class FakeOKConnection(OKConnection, connection.FakeConnection):
    """A Fake OpalKelly Connection: returns success on all messages"""

    def hasDevice(self):
        return True

    def _createDevice(self):
        """Creates a new connection handle"""
        # self._setDevice(fp.FrontPanel())
        return self.getDevice()

    def _configure(self, bitFile=None):
        return 0

    def _connect(self, serialId=None):
        return 0

    def _disconnect(self):
        return 0

    def _trigger(self):
        return 0

    def _update(self):
        return 0

    def _wireIn(self, address, value):
        return 0



