"""A small demo script, demonstrating the working of the connection driver"""

from pycultivator_led_panel.connection import okConnection
from pycultivator_lab.scripts import script
import argparse
from time import sleep


class DemoScript(script.Script):

    def __init__(self):
        super(DemoScript, self).__init__()
        self._device = okConnection.OKConnection()
        self._lamp = 0
        self._pumps = {}

    def getLamp(self):
        return self._lamp

    def setLamp(self, lamp):
        self._lamp = lamp

    @property
    def lamp(self):
        return self.getLamp()

    @lamp.setter
    def lamp(self, l):
        if l not in range(8):
            raise ValueError("Invalid value for lamp: {}".format(l))
        self.lamp = l

    def getDevice(self):
        """ The connection driver to the Opal Kelly FPGA

        :return:
        :rtype: pycultivator_led_panel.connection.okConnection.OKConnection
        """
        return self._device

    @property
    def device(self):
        return self.getDevice()

    def _prepare(self):
        result = False
        result = self.getDevice().connect()
        self.getLog().info("Connected to Panel: {}".format(result))
        if result:
            self.configure()
        return result

    def configure(self):
        result = self.getDevice().setALlPulseWidth(self.getLamp(), 0)
        result = self.getDevice().setAllIntensity(self.getLamp(), 0) and result
        return result

    def execute(self):
        self.set_led(0, 100, 100)
        self.set_led(1, 50, 50)
        self.set_led(2, 100, 100)
        i = 4
        self.log("Sleep for {} seconds..".format(i), end="")
        for i in range(i+1):
            self.log("Sleep for {} seconds..".format(4-i), start="\r", end="")
            sleep(1)
        self.log("Sleep for {} seconds..".format(4-i))
        for i in range(3):
            self.set_led(i, 0, 0)
        return True

    def set_led(self, led, intensity=None, pulse_width=None):
        result = True
        if intensity is not None:
            result = self.set_intensity(led, intensity)
        if pulse_width is not None:
            result = self.set_pulse_width(led, pulse_width) and result
        return result

    def set_intensity(self, led, intensity):
        lamp = self.getLamp()
        return self.getDevice().setIntensity(lamp, led, intensity)

    def set_pulse_width(self, led, pulse_width):
        lamp = self.getLamp()
        return self.getDevice().setPulseWidth(lamp, led, pulse_width)

    def _clean(self):
        result = True
        if self.getDevice().isConnected():
            result = self.getDevice().disconnect()
            self.getLog().info("Disconnected from Panel: {}".format(result))
        result = super(DemoScript, self)._clean() and result
        return result

    def default_arguments(self):
        defaults = super(DemoScript, self).default_arguments()
        defaults["lamp"] = self.getLamp()
        return defaults

    def define_arguments(self, parser=None):
        if parser is None:
            parser = argparse.ArgumentParser(description="Demo application of the LED Panel")
        parser = super(DemoScript, self).define_arguments(parser)
        parser.add_argument(
            "--lamp", type=int, help="Set index of the lamp"
        )
        return parser

    def parse_arguments(self, arguments):
        super(DemoScript, self).parse_arguments(arguments)
        lamp = arguments.get("lamp")
        if lamp is None:
            lamp = self.getLamp()
        self.setLamp(lamp)

if __name__ == "__main__":
    ds = DemoScript()
    # create argument parser
    p = ds.define_arguments()
    # load defaults
    a = ds.default_arguments()
    # load arguments and update defaults
    a.update(vars(p.parse_args()))
    # parse arguments
    ds.parse_arguments(a)
    # run script
    ds.run()
