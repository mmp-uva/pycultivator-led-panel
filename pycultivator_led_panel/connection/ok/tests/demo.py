# from pycultivator_led_panel.connection.ok import ok
from okfrontpanel import fp
import os


class OpalKellyConnection(object):

    def __init__(self):
        # self.device = ok.okCFrontPanel()
        self.device = fp.FrontPanel()
        self.connect()

    def __del__(self):
        if self.device.IsOpen():
            self.disconnect()

    def connect(self):
        if self.device.IsOpen():
            raise Exception("Connection is already open")
        self.device.OpenBySerial("")

    def disconnect(self):
        if not self.device.IsOpen():
            raise Exception("Connection is already closed")
        self.device.Close()

    def configure(self, path):
        if not os.path.exists(path):
            raise Exception("Bit file does not exists!")
        result = self.device.ConfigureFPGA(path)
        if result < 0:
            raise Exception("Failed to configure FPGA: {}".format(result))
        return result == 0

    def setPulseWidth(self, led, value):
        self.device.SetWireInValue(0x11, led)
        self.device.SetWireInValue(0x10, value)
        self.apply()

    def setIntensity(self, led, value):
        self.device.SetWireInValue(0x11, led+8)
        self.device.SetWireInValue(0x10, value)
        self.apply()

    def apply(self):
        self.update()
        self.trigger()

    def trigger(self):
        self.device.ActivateTriggerIn(0x40, 0)

    def update(self):
        self.device.UpdateWireIns()


if __name__ == "__main__":

    conn = OpalKellyConnection()
    # bitFile
    bitFile = os.path.join(os.path.dirname(__file__), "..", "ow_pwm.bit")
    if not os.path.exists(bitFile):
        print("Unable to load bitFile: File does not exist.")
        exit(-1)
    # configure
    print("Configure FPGA")
    conn.configure(bitFile)
    print("Set intensity")
    for i in range(8):
        conn.setPulseWidth(i, 0)
        conn.setIntensity(i, 0)

