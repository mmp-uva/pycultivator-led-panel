"""This module provides an API for serializing the configuration setting of the LED Panel"""

from pycultivator_led_panel.config import baseConfig as baseConfig
from pycultivator.config import xmlConfig as baseXMLConfig
import os

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class XMLPanelConfig(baseXMLConfig.XMLDeviceConfig, baseConfig.PanelConfig):
    """Config class to create/save/update the device objects from/to the configuration source"""

    XML_ELEMENT_TAG = "panel"
    required_source = "pycultivator_led_panel.config.xmlConfig.XMLConfig"


class XMLConnectionConfig(baseXMLConfig.XMLConnectionConfig, baseConfig.ConnectionConfig):
    """Config class for handling the configuration of a LabJack connection object from a configuration source"""

    required_source = "pycultivator_led_panel.config.xmlConfig.XMLConfig"


class XMLPanelInstrumentConfig(baseXMLConfig.XMLInstrumentConfig, baseConfig.PanelInstrumentConfig):

    required_source = "pycultivator_led_panel.config.xmlConfig.XMLConfig"

    def read_properties(self, definition, obj):
        obj = super(XMLPanelInstrumentConfig, self).read_properties(definition, obj)
        address = self.xml.getElementAttribute(definition, "address", _type=int)
        if address is not None:
            obj.setAddress(address)
        return obj

    def write_properties(self, obj, definition):
        result = super(XMLPanelInstrumentConfig, self).write_properties(obj, definition)
        if result:
            self.xml.setElementAttribute(definition, "address", obj.getAddress())
        return result


class XMLLEDConfig(XMLPanelInstrumentConfig, baseConfig.LEDConfig):
    """Config class to handle the LED configuration from the configuration source"""

    XML_ELEMENT_TAG = "led"
    XML_ELEMENT_SCOPE = XMLPanelInstrumentConfig.XML_LOCAL_DEEP_SCOPE
    required_source = "pycultivator_led_panel.config.xmlConfig.XMLConfig"

    def read_properties(self, definition, obj):
        obj = super(XMLPanelInstrumentConfig, self).read_properties(definition, obj)
        """:type: pycultivator_led_panel.panelLED.PanelLED"""
        wavelength = self.xml.getElementAttribute(definition, "wavelength", _type=float, default=0.0)
        obj.setWavelength(wavelength)
        color = self.xml.getElementAttribute(definition, "color", _type=str)
        if color is not None:
            obj.setColor(color)
        return obj

    def write_properties(self, obj, definition):
        """Write LED properties to the configuration source

        :type obj: pycultivator_led_panel.panelLED.PanelLED
        :type definition: lxml.etree._Element._Element
        :return:
        """
        result = super(XMLLEDConfig, self).write_properties(obj, definition)
        if result:
            self.xml.setElementAttribute(definition, "wavelength", obj.wavelength)
            self.xml.setElementAttribute(definition, "color", obj.color)
        return result


class XMLConfig(baseXMLConfig.XMLConfig, baseConfig.Config):
    """Config class for handling the Light Calibration Plate XML File"""

    # set available helpers
    _known_helpers = [
        # baseXMLConfig.XMLObjectConfig,
        # baseXMLConfig.XMLPartConfig,
        XMLPanelConfig,
        XMLConnectionConfig,
        XMLLEDConfig,
        baseXMLConfig.XMLCalibrationConfig,
        baseXMLConfig.XMLPolynomialConfig,
        baseXMLConfig.XMLSettingsConfig,
    ]

    XML_SCHEMA_PATH = os.path.join("schema", "pycultivator_led_panel.xsd")
    XML_SCHEMA_COMPATIBILITY = {
        "1.1": 2,
        "1.0": 0,
        # version: level
        # level 2: full support
        # level 1: deprecated
        # level 0: not supported
    }

    @classmethod
    def load(cls, path, settings=None, **kwargs):
        """Loads the source into memory and creates the config object

        :param path: Location from which the configuration will be loaded
        :type path: str
        :param settings: Settings dictionary that should be loaded into the configuration
        :type settings: None or dict[str, object]
        :rtype: None or pycultivator_led_panel.config.Config.Config
        """
        return super(XMLConfig, cls).load(path, settings, **kwargs)


class XMLConfigException(baseConfig.ConfigException):
    """An Exception raised by the XMlConfig classes"""

    def __init__(self, msg):
        super(XMLConfigException, self).__init__(msg)
