"""This module provides an API for serializing the configuration setting of the 24-well plate incubator"""

from pycultivator.config import baseConfig
from pycultivator_led_panel import panel
from pycultivator_led_panel.connection import okConnection
from pycultivator_led_panel.panelLED import PanelLED

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class PanelConfig(baseConfig.DeviceConfig):
    """Config class to create/save/update the device objects from/to the configuration source"""

    _configures = panel.Panel
    _configures_types = {"panel"}
    required_source = "pycultivator_led_panel.config.baseConfig.Config"

    # def read_instruments(self, definition, obj, template=None):
    #     helper = self.getSource().getHelperFor("led")
    #     """:type: pycultivator_led_panel.baseconfig.Config.LEDConfig"""
    #     instruments = []
    #     if helper is not None:
    #         instruments = helper.load_all(definition)
    #     for i in instruments:
    #         obj.addInstrument(i)
    #     return obj


class ConnectionConfig(baseConfig.ConnectionConfig):
    """Config class for handling the configuration of a connection object from a configuration source"""

    _configures = okConnection.OKConnection
    required_source = "pycultivator_led_panel.config.baseConfig.Config"


class PanelInstrumentConfig(baseConfig.InstrumentConfig):

    required_source = "pycultivator_led_panel.config.baseConfig.Config"


class LEDConfig(PanelInstrumentConfig):
    """Config class to handle the LED configuration from the configuration source"""

    _configures = PanelLED
    _configures_types = {"led"}
    required_source = "pycultivator_led_panel.config.baseConfig.Config"


class Config(baseConfig.Config):
    """ The config class provides the API to configure a light calibration plate object."""

    namespace = "config"
    # set available helpers
    _known_helpers = [
        PanelConfig,
        ConnectionConfig,
        LEDConfig,
        baseConfig.CalibrationConfig,
        baseConfig.PolynomialConfig,
        baseConfig.SettingsConfig
    ]

    default_settings = {
        # default settings for this class
    }

    def __init__(self, settings=None, **kwargs):
        super(Config, self).__init__(settings, **kwargs)


class ConfigException(baseConfig.ConfigException):
    """An Exception raised by the XMlConfig classes"""

    def __init__(self, msg):
        super(ConfigException, self).__init__(msg)
