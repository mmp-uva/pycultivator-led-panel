"""Module for testing the pycultivator_led_panel XML Configuration"""

from pycultivator_led_panel.config import xmlConfig
from pycultivator.config.tests import test_xmlConfig
from pycultivator.core import pcXML
import test_baseConfig
import os

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'

SCHEMA_DIR = os.path.join(os.path.abspath(os.path.dirname(__file__)), "..", "schema")
CONFIG_PATH = os.path.join(SCHEMA_DIR, "configuration.xml")
SCHEMA_PATH = os.path.join(SCHEMA_DIR, "pycultivator_led_panel.xsd")


class TestXMLObjectConfig(test_xmlConfig.TestXMLObjectConfig):

    _abstract = True
    config_location = CONFIG_PATH


class TestXMLPanelConfig(
    TestXMLObjectConfig,
    test_xmlConfig.TestXMLDeviceConfig,
    test_baseConfig.TestPanelConfig
):

    _abstract = False
    _subject_cls = xmlConfig.XMLPanelConfig

    def getSubject(self):
        """Return the subject being tested

        :return:
        :rtype: pycultivator_led_panel.config.xmlConfig.XMLPanelConfig
        """
        return super(TestXMLPanelConfig, self).getSubject()

    def test_select(self):
        xml = xmlConfig.baseXMLConfig.pcXML.et.XML(
            """<panel xmlns="pyCultivator" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:pc="pyCultivator"
            targetNamespace="pyCultivator">
            <instruments><led></led><led></led></instruments></panel>"""
        )
        elements = self.getSubject().select(xml)
        self.assertEqual(len(elements), 1)
        xml = pcXML.et.XML(
            """<a xmlns="pyCultivator" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:pc="pyCultivator"
            targetNamespace="pyCultivator">
            </a>"""
        )
        elements = self.getSubject().select(xml)
        self.assertEqual(len(elements), 0)

    def test_is_valid_tag(self):
        e = pcXML.et.XML("<panel></panel>")
        self.assertTrue(self.subject.is_valid_tag(e))
        e = pcXML.et.XML("<bla></bla>")
        self.assertFalse(self.subject.is_valid_tag(e))
        root = self.getConfiguration().getXML().getRoot()[0]
        self.assertTrue(self.subject.is_valid_tag(root))

    def test_create_definition(self):
        xml = pcXML.et.XML(
            """<a xmlns="pyCultivator" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:pc="pyCultivator"
            targetNamespace="pyCultivator">
                </a>""")
        panel = self.getDefaultObject()
        element = self.subject.create_definition(panel, root=xml)
        self.assertIsNotNone(element)
        self.assertEqual(self.subject.getXML().getCleanElementTag(element), self.subject.XML_ELEMENT_TAG)
        panels = self.subject.select(root=xml, scope="//")
        self.assertEqual(len(panels), 1)
        connections = self.getSubject().find_elements("connection", root=xml, scope="//")
        self.assertEqual(len(connections), 0)
        instruments = self.getSubject().find_elements("instruments", root=xml, scope="//")
        self.assertEqual(len(instruments), 0)

    def test_save(self):
        xml = pcXML.et.XML(
            """<a xmlns="pyCultivator" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:pc="pyCultivator"
            targetNamespace="pyCultivator">
            </a>"""
        )
        panel = self.getDefaultObject()
        self.subject.save(panel, root=xml)
        elements = self.subject.select(root=xml, scope="//")
        self.assertEqual(len(elements), 1)
        element = elements[0]
        self.assertEqual(self.subject.getXML().getCleanElementTag(element), self.subject.XML_ELEMENT_TAG)
        panels = self.getSubject().select(root=xml, scope="//")
        self.assertEqual(len(panels), 1)
        connections = self.getSubject().find_elements("connection", root=xml, scope="//")
        self.assertEqual(len(connections), 0)
        instruments = self.getSubject().find_elements("instruments", root=xml, scope="//")
        self.assertEqual(len(instruments), 1)

    def test_write(self):
        xml = pcXML.et.XML(
            """<a xmlns="pyCultivator" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:pc="pyCultivator"
            targetNamespace="pyCultivator">
            </a>"""
        )
        panel = self.getDefaultObject()
        element = self.getSubject().create_definition(panel, root=xml)
        panels = self.getSubject().select(root=xml, scope="//")
        self.assertEqual(len(panels), 1)
        connections = self.getSubject().find_elements("connection", root=xml, scope="//")
        self.assertEqual(len(connections), 0)
        instruments = self.getSubject().find_elements("instruments", root=xml, scope="//")
        self.assertEqual(len(instruments), 0)
        self.assertIsNotNone(element)
        self.subject.write(panel, element)
        connections = self.getSubject().find_elements("connection", root=xml, scope="//")
        self.assertEqual(len(connections), 0)
        instruments = self.getSubject().find_elements("instruments", root=xml, scope="//")
        self.assertEqual(len(instruments), 1)

    def test_read(self):
        pass


def test_retrieve(self):
        xml = xmlConfig.baseXMLConfig.pcXML.et.XML(
            """<a xmlns="pyCultivator" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:pc="pyCultivator"
            targetNamespace="pyCultivator">
                <led id="1"></led><led></led></a>"""
        )
        led = self.getDefaultObject()
        led.name = 1
        element = self.subject.retrieve(led, root=xml)
        self.assertIsNotNone(element)
        # retrieve first led definition
        elements = self.subject.select()
        self.assertGreaterEqual(len(elements), 1)
        e = elements[0]
        led = self.getSubject().read(e, self.getDefaultObject())
        self.assertIsNotNone(led)
        self.assertEqual(led.name, '0')
        # see if we retrieve the correct element
        element = self.subject.retrieve(led)
        self.assertIsNotNone(element)
        self.assertEqual(self.subject.read(element, self.getDefaultObject()).name, led.name)
        self.assertEqual(self.subject.read(element, self.getDefaultObject()).color, led.color)

class TestXMLLEDConfig(
    test_baseConfig.TestLEDConfig,
    TestXMLObjectConfig,
    test_xmlConfig.TestXMLInstrumentConfig,
):
    """Class for testing the XMLSettingsConfig object"""

    _abstract = False
    _subject_cls = xmlConfig.XMLLEDConfig

    def getSubject(self):
        """

        :return:
        :rtype: pycultivator_led_panel.config.xmlConfig.XMLLEDConfig
        """
        return super(TestXMLLEDConfig, self).getSubject()

    def test_select(self):
        xml = xmlConfig.baseXMLConfig.pcXML.et.XML(
            """<a xmlns="pyCultivator" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:pc="pyCultivator"
            targetNamespace="pyCultivator">
                <led></led><led></led></a>"""
        )
        elements = self.getSubject().select(xml)
        self.assertEqual(len(elements), 2)
        xml = pcXML.et.XML(
            """<a xmlns="pyCultivator" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:pc="pyCultivator"
            targetNamespace="pyCultivator">
            </a>"""
        )
        elements = self.getSubject().select(xml)
        self.assertEqual(len(elements), 0)

    def test_read(self):
        elements = self.getSubject().select(scope="//")
        self.assertGreaterEqual(len(elements), 8)
        element = elements[0]
        led = self.subject.read(element, self.getDefaultObject())
        self.assertIsInstance(led, self.getSubjectClass().configures())

    def test_create(self):
        xml = pcXML.et.XML(
            """<a xmlns="pyCultivator" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:pc="pyCultivator"
            targetNamespace="pyCultivator">
                </a>""")
        led = self.getDefaultObject()
        element = self.subject.create_definition(led, root=xml)
        self.assertIsNotNone(element)
        self.assertEqual(self.subject.getXML().getCleanElementTag(element), self.subject.XML_ELEMENT_TAG)
        leds = self.getSubject().select(root=xml, scope="//")
        self.assertEqual(len(leds), 1)

    def test_write(self):
        xml = pcXML.et.XML(
            """<a xmlns="pyCultivator" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:pc="pyCultivator"
            targetNamespace="pyCultivator">
                </a>""")
        led = self.getDefaultObject()
        element = self.getSubject().create_definition(led, xml)
        self.assertIsNotNone(element)
        # now write information
        self.subject.write(led, element)
        # check info in element
        elements = self.getSubject().select(root=xml, scope="//")
        self.assertEqual(len(elements), 1)
        self.assertEqual(self.subject.getXML().getElementId(elements[0]), str(led.name))
        xml = pcXML.et.XML(
            """<a xmlns="pyCultivator" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:pc="pyCultivator"
            targetNamespace="pyCultivator">
                </a>""")
        led = self.getDefaultObject()
        led.color = "yellow"
        self.assertEqual(led.color, "yellow")
        element = self.getSubject().create_definition(led, xml)
        self.assertIsNotNone(element)
        # now write information
        self.subject.write(led, element)
        # check info in element
        elements = self.getSubject().select(root=xml, scope="//")
        self.assertEqual(len(elements), 1)
        self.assertEqual(self.subject.getXML().getElementAttribute(elements[0], "color"), led.color)

    def test_save(self):
        led = self.getDefaultObject()
        xpath = "//pc:instruments/pc:led"
        s = self.getSubject().getXML()
        # test save to existing element with matching id
        xml = pcXML.et.XML(
            """<panel xmlns="pyCultivator" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:pc="pyCultivator"
            targetNamespace="pyCultivator">
            <instruments><led id="0"/></instruments></panel>""")
        i_element = s.getElementsByXPath("//pc:instruments", root=xml, prefix="pc")[0]
        elements = s.getElementsByXPath(xpath, root=xml, prefix="pc")
        self.assertEqual(len(elements), 1)
        element = elements[0]
        self.subject.save(led, element)
        # use find directly under root
        elements = self.subject.select(root=xml, scope="/")
        self.assertEqual(len(elements), 0)
        # use find under instruments
        elements = self.subject.select(root=i_element)
        self.assertEqual(len(elements), 1)
        self.assertEqual(self.subject.getXML().getElementId(elements[0]), str(led.name))
        # test save to existing element without id
        xml = pcXML.et.XML(
            """<panel xmlns="pyCultivator" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:pc="pyCultivator"
            targetNamespace="pyCultivator">
            <instruments><led/></instruments></panel>""")
        elements = s.getElementsByXPath(xpath, root=xml, prefix="pc")
        self.assertEqual(len(elements), 1)
        element = elements[0]
        self.subject.save(led, element)
        elements = self.subject.select(root=xml, scope="//")
        self.assertEqual(len(elements), 1)
        self.assertEqual(self.subject.getXML().getElementId(elements[0]), str(led.name))
        # test save to existing element without matching id
        xml = pcXML.et.XML(
            """<panel xmlns="pyCultivator" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:pc="pyCultivator"
            targetNamespace="pyCultivator">
            <instruments><led id="1"/></instruments></panel>""")
        self.subject.save(led, xml)
        elements = s.getElementsByXPath(xpath, root=xml, prefix="pc")
        self.assertEqual(len(elements), 2)
        # test retrieve of the correct element
        o = self.subject.find_first(led, root=i_element)
        self.assertIsNotNone(o)
        self.assertEqual(self.subject.getXML().getElementId(o), str(led.name))
        # test save to document without existing element
        xml = pcXML.et.XML(
            """<panel xmlns="pyCultivator" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:pc="pyCultivator"
            targetNamespace="pyCultivator">
            </panel>""")
        self.subject.save(led, xml)
        # check if there is an instruments element
        elements = s.getElementsByXPath("//pc:instruments", root=xml, prefix="pc")
        self.assertEqual(len(elements), 1)
        # check if we can find the element
        leds = self.subject.select(root=xml, scope="//")
        self.assertEqual(len(leds), 1)
        self.assertEqual(self.subject.getXML().getElementId(leds[0]), str(led.name))
        # test save to document with existing element matching id
        xml = pcXML.et.XML(
            """<panel xmlns="pyCultivator" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:pc="pyCultivator"
            targetNamespace="pyCultivator">
            <led id="0"/></panel>""")
        self.subject.save(led, xml)
        leds = self.subject.select(root=xml, scope="//")
        self.assertEqual(len(leds), 1)
        self.assertEqual(self.subject.getXML().getElementId(leds[0]), str(led.name))
        # test with different root
        xml = pcXML.et.XML(
            """<a xmlns="pyCultivator" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:pc="pyCultivator"
            targetNamespace="pyCultivator"></a>""")
        self.subject.save(led, xml)
        elements = s.getElementsByXPath(xpath, root=xml, prefix="pc")
        self.assertEqual(len(elements), 1)
        elements = self.subject.select(root=xml)
        self.assertEqual(len(elements), 1)
        self.assertEqual(self.subject.getXML().getElementId(elements[0]), str(led.name))
