"""
Module testing the pycultivator_led_panel configuration classes
"""

from pycultivator.config.tests import test_baseConfig
from pycultivator_led_panel.config import baseConfig

__author__ = 'Joeri Jongbloets <j.a.jongbloets@uva.nl>'


class SubjectTestConfig(test_baseConfig.TestConfig):
    """Class for testing the pycultivator Config object"""

    _abstract = True
    _subject_cls = baseConfig.Config


class TestPanelConfig(test_baseConfig.TestDeviceConfig):
    """Class for testing the pycultivator ComplexDeviceConfig object"""

    _abstract = True
    _subject_cls = baseConfig.PanelConfig

    def getDefaultObject(self):
        return self.subject.default_class(0)


class TestConnectionConfig(test_baseConfig.TestConnectionConfig):
    """"""

    _abstract = True
    _subject_cls = baseConfig.ConnectionConfig


class TestLEDConfig(test_baseConfig.TestInstrumentConfig):
    """Class for testing the XMLComplexDeviceConfig object"""

    _abstract = True
    _subject_cls = baseConfig.LEDConfig

    def getDefaultObject(self):
        return self.subject.default_class(0)
